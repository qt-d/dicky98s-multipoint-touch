import Qt 4.6

ListModel {
    id: menuModel
    ListElement {
        name: "Bill Jones"
        icon: "pics/qtlogo-64.png"
    }
    ListElement {
        name: "Jane Doe"
        icon: "pics/qtlogo-64.png"
    }
    ListElement {
        name: "John Smith"
        icon: "pics/qtlogo-64.png"
    }
}
