import People 1.0

// ![0]
BirthdayParty {
    celebrant: Boy {
        name: "Bob Jones"
        shoeSize: 12
    }

    Boy { name: "Joan Hodges" }
    Boy { name: "Jack Smith" }
    Girl { name: "Anne Brown" }
}
// ![0]
