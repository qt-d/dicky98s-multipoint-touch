import Qt 4.6

QtObject {
    property int a
    property bool b
    property double c
    property real d
    property string e
    property url f
    property color g
    property date h
    property var i
    property variant j
}

