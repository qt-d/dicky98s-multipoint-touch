TEMPLATE=subdirs
SUBDIRS=\
    qabstractvideobuffer \
    qabstractvideosurface \
    qaudiodeviceinfo \
    qaudioformat \
    qaudioinput \
    qaudiooutput \
    qsoundeffect \
    qdeclarativeaudio \
    qdeclarativevideo \
    qgraphicsvideoitem \
    qmediacontent \
    qmediaobject \
    qmediaplayer \
    qmediaplaylist \
    qmediaplaylistnavigator \
    qmediapluginloader \
    qmediaresource \
    qmediaservice \
    qmediaserviceprovider \
    qmediatimerange \
    qvideoframe \
    qvideosurfaceformat \
    qvideowidget \

