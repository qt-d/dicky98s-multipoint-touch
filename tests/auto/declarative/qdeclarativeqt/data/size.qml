import Qt 4.6

QtObject {
    property var test1: Qt.size(19, 34);
    property var test2: Qt.size(3, 109.2);
    property var test3: Qt.size(-3, 10);
    property var test4: Qt.size(3);
    property var test5: Qt.size(3, 109.2, 1);
}


