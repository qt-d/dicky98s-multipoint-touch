import Qt 4.6

QtObject {
    property var test1: Qt.vector3d(1, 0, 0.9);
    property var test2: Qt.vector3d(102, -10, -982.1);
    property var test3: Qt.vector3d(102, -10);
    property var test4: Qt.vector3d(102, -10, -982.1, 10);
}
