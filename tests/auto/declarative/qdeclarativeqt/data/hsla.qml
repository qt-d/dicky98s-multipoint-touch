import Qt 4.6

QtObject {
    property color test1: Qt.hsla(1, 0, 0, 0.8);
    property color test2: Qt.hsla(1, 0.5, 0.3);
    property color test3: Qt.hsla(1, 1);
    property color test4: Qt.hsla(1, 1, 1, 1, 1);
    property color test5: Qt.hsla(1.2, 1, 1);
    property color test6: Qt.hsla(-0.1, 1, 1);
}

