import Qt 4.6

QtObject {
    property var test1: Qt.point(19, 34);
    property var test2: Qt.point(-3, 109.2);
    property var test3: Qt.point(-3);
    property var test4: Qt.point(-3, 109.2, 1);
}

