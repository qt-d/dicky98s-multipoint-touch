import Qt 4.6

SmoothedAnimation {
    to: 10; velocity: 250; reversingMode: SmoothedAnimation.Sync
    maximumEasingTime: 150
}
