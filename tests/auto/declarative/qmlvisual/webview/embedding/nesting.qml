import Qt 4.6
import org.webkit 1.0

WebView {
    width: 300
    height: 200
    url: "nesting.html"
    settings.pluginsEnabled: true
}
